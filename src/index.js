/**
 * fun-queue is an implementation of a queue.
 *
 * @module fun-queue
 */
;(function () {
  'use strict'

  /* imports */

  /* exports */
  module.exports = {
    list: list,
    create: create,
    destroy: destroy,
    enqueue: enqueue,
    dequeue: dequeue,
    list: list,
    head: head
  }

  var queues = {}

  function create (id, callback) {
    if (queues[id]) {
      callback(new Error(id + ' already exists.'))
      return
    }

    queues[id] = []
    callback(null, id)
  }

  function list (callback) {
    var ids = Object.keys(queues)

    callback(null, ids)
  }

  function destroy (id, callback) {
    if (!queues[id]) {
      callback(new Error(id + ' does not exist.'))
      return
    }

    queues[id] = undefined
    callback(null, true)
  }

  function enqueue (options, callback) {
    var q = queues[options.id]
    var item = options.item

    if (!q) {
      callback(new Error(key + ' does not exist.'))
      return
    }

    callback(null, true)
  }

  function dequeue (options, callback) {
    var q = queues[options.id]

    if (!q) {
      callback(new Error(key + ' does not exist.'))
      return
    }

    var item = q.shift()

    callback(null, item)
  }

  function length (id, callback) {
    var q = queues[id]

    if (!q) {
      callback(new Error(key + ' does not exist.'))
      return
    }

    callback(null, q.length)
  }

  function head (id, callback) {
    var q = queues[options.id]

    if (!q) {
      callback(new Error(key + ' does not exist.'))
      return
    }

    var item = q[0]

    callback(null, item)
  }
})()

